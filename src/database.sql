-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema duosystem
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema duosystem
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `duosystem` DEFAULT CHARACTER SET utf8 ;
USE `duosystem` ;

-- -----------------------------------------------------
-- Table `duosystem`.`status`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `duosystem`.`status` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `status` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `duosystem`.`atividade`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `duosystem`.`atividade` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `nome` VARCHAR(255) NOT NULL,
  `descricao` TEXT(600) NOT NULL,
  `data_inicio` DATE NOT NULL,
  `data_fim` DATE NULL,
  `status_id` INT UNSIGNED NOT NULL,
  `situacao` VARCHAR(7) NULL DEFAULT 'ativo',
  PRIMARY KEY (`id`),
  INDEX `fk_atividade_status_idx` (`status_id` ASC),
  CONSTRAINT `fk_atividade_status`
    FOREIGN KEY (`status_id`)
    REFERENCES `duosystem`.`status` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

-- -----------------------------------------------------
-- Inserts Table `duosystem`.`status`
-- -----------------------------------------------------
INSERT INTO `status` (`id`, `status`) VALUES (NULL, 'Pendente'), (NULL, 'Em Desenvolvimento'), (NULL, 'Em Teste'), (NULL, 'Concluído');