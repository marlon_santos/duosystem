<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Atividades extends CI_Controller {
    
    public function __construct() {
        parent::__construct();
        $this->load->model('atividades_model');
        $this->load->model('status_model');
        $this->load->helper('url');
        $this->load->helper('form');
    }
    
    public function index(){
        $data["title"] = "Listar Atividades";
        $data["rows"] = $this->atividades_model->get_atividade();
        $data["status"] = $this->status_model->get_status();
        $data["status_options"] = array_merge(array('-'=>'Todos'),$this->status_model->get_status());
        $data["situacao_options"] = array('-'=>'Todos','ativo'=>'Ativo', 'inativo'=>'Inativo');
        $this->load->view('inc/header',$data);
        $this->load->view('home',$data);
        $this->load->view('inc/footer');
    }
    
    public function date_validation($date) {
        if (trim($date)=='') return TRUE; //Permite datas vazias
        $arr = explode('/',$date);
        $day = (isset($arr[0]))?(int) $arr[0]:00;
        $month = (isset($arr[1]))?(int) $arr[1]:00;
        $year = (isset($arr[2]))?(int) $arr[2]:0000;
        
        //Verificação de data em formato válido
        if (checkdate($month, $day, $year) === FALSE){
            $this->form_validation->set_message('date_validation','O campo {field} deve conter um formato válido.');
            return FALSE;
        } else {
            return TRUE;
        }
    }
    
    public function create_update($id=''){
        $this->load->library('form_validation');
        
        //Regras de validação
        $this->form_validation->set_rules('nome','Nome','trim|required|max_length[255]');
        $this->form_validation->set_rules('descricao','Descrição','trim|required|max_length[600]');
        $this->form_validation->set_rules('data_inicio','Data de Início','trim|required|callback_date_validation');
        $this->form_validation->set_rules('data_fim','Data de Fim','trim|callback_date_validation');
        
        if ($this->form_validation->run() == FALSE) {
            $data['form_error'] = strip_tags(validation_errors());
        } else {
            $data['form_error'] = NULL;
            if (is_numeric($id)) {
                $this->atividades_model->set_atividade($id);
            } else {
                $id = $this->atividades_model->set_atividade();
            }
        }
        //Populando dados para exibição na view
        $data['class_validation_errors'] = (strlen($data['form_error']))?" show_validation_errors":" hide_validation_errors";
        $data["title"] = "Cadastrar Atividade";
        $data["status_options"] = $this->status_model->get_status();
        $data["situacao_options"] = array('ativo'=>'Ativo', 'inativo'=>'Inativo');
        $data["input_attr"] = array("class"=>"form-control");
        if (is_numeric($id)) {
            $data["row"] = $this->atividades_model->get_atividade($id);
            $data["row"]["data_inicio"] = date('d/m/Y',strtotime($data["row"]["data_inicio"]));
            $data["row"]["data_fim"] = (strlen($data["row"]["data_fim"]))?date('d/m/Y',strtotime($data["row"]["data_fim"])):'';
            $data["status"] = $this->status_model->get_status($data["row"]["status_id"]);
            $data["id"] = $id;
        }
        
        //Carregando view
        $this->load->view('inc/header',$data);
        $this->load->view('formulario',$data);
        $this->load->view('inc/footer');
    }
}
