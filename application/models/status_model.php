<?php
class Status_model extends CI_Model {

    public function __construct()
    {
        $this->load->database();
    }
    
    public function get_status($id="") {
        if (!is_numeric($id)) {
            $query = $this->db->get('status');
            $return_array = array();
            foreach ($query->result_array() as $key => $row) {
                $return_array[$row['id']] = $row['status'];
            }
            return $return_array;
        }
        
        $query = $this->db->get_where('status', array('id' => $id));
        $return_value = $query->row_array();
        return $return_value['status'];
    }
}

