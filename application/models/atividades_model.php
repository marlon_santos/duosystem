<?php
class Atividades_model extends CI_Model {

    public function __construct()
    {
        $this->load->database();
    }
    
    public function set_atividade($id="") {
        $this->load->helper('date');
        
        //Formatação das datas
        $data_inicio = mdate('%Y-%m-%d',strtotime(str_replace('/','-',$this->input->post('data_inicio'))));
        $data_fim = (strlen($this->input->post('data_fim')))?mdate('%Y-%m-%d',strtotime(str_replace('/','-',$this->input->post('data_fim')))):null; //Permite data de fim vazia
        
        $data = array(
            'nome' => $this->input->post('nome'),
            'descricao' => $this->input->post('descricao'),
            'data_inicio' => $data_inicio,
            'data_fim' => $data_fim,
            'status_id' => intval($this->input->post('status')),
            'situacao' => $this->input->post('situacao')
        );
        
        if (is_numeric($id)) {
            //Se existe um id preenchido, realiza o update
            $this->db->where('id', $id);
            return $this->db->update('atividade',$data);
        } else {
            //Se não existe um id preenchido, insere um novo registro
            $this->db->insert('atividade',$data);
            return $this->db->insert_id();
        }
    }
    
    public function get_atividade($id="") {
        if (!is_numeric($id)) {
            //Se não houver um id preenchido busca todos os registros da tabela e retorna em um array
            $query = $this->db->get('atividade');
            return $query->result_array();
        }
        
        //Caso haja um id retorna apenas o registro necessário
        $query = $this->db->get_where('atividade', array('id' => $id));
        return $query->row_array();
    }
}
