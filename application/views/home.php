<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
    <h5>Filtros:</h5>
    <div class="row">
        <div class="col">
<?php
$status_attr = array('class' => 'form-control', 'id' => 'searchInput-1');
echo '<div class="form-group">';
echo form_label('Status','status');
echo form_dropdown('status', $status_options,'', $status_attr);
echo '</div>';
?>
        </div>
        <div class="col">
<?php
$situacao_attr = array('class' => 'form-control', 'id' => 'searchInput-2');
echo '<div class="form-group">';
echo form_label('Situação','situacao');
echo form_dropdown('situacao', $situacao_options,'', $situacao_attr);
echo '</div>';
?>    
        </div>
    </div>
    <table id="atividades" class="table table-sm table-hover">
        <thead>
            <th>#</th> 
            <th>Nome</th> 
            <th>Data de Início</th> 
            <th>Data de Fim</th> 
            <th>Status</th> 
            <th>Situação</th>
            <th>&nbsp;</th>
        </thead>
        <tbody>
            <?php
            if (count($rows)) {
                foreach ($rows as $key => $row) {
                    $data_fim = (strlen(trim($row['data_fim'])))?date('d/m/Y',strtotime($row['data_fim'])):'-'
            ?>
            <tr<?php if (mb_strtolower($status[$row['status_id']])=='concluído') echo ' class="table-success"';?>>
                <td><?php echo $row['id'];?></td>
                <td><?php echo $row['nome'];?></td>
                <td><?php echo date('d/m/Y',strtotime($row['data_inicio']));?></td>
                <td><?php echo $data_fim;?></td>
                <td><?php echo $status[$row['status_id']];?></td>
                <td><?php echo $row['situacao'];?></td>
                <td><button type="button" class="btn btn-outline-primary btn-sm editar" data-id="<?php echo $row['id'];?>">Editar</button></td>
            </tr>
            <?php
                }
            } else {
            ?>
            <tr>
                <td colspan="7" class='td_centered'>Não existem atividades cadastradas</td>
            </tr>
            <?php    
            }
            ?>
        </tbody>
    </table>
    <div style="text-align: right"><button type="button" class="btn btn-success novo">Nova Atividade</button></div>
