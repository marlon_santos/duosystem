<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$form_action = "registro";
if (isset($id)) {
    $form_action .= '/'.$id;
}
echo form_open($form_action, array('id'=>'form_atividade'));
?>

<?php
$values = array();
foreach (array('nome','descricao','data_inicio','data_fim','status_id','situacao') as $key => $value) {
    $values[$value] = (isset($row[$value]))?$row[$value]:set_value($value);
}
?>
<div class="alert alert-warning<?php echo $class_validation_errors?>" role="alert" id="validation_errors">
<?php
    echo $form_error;
?>
</div>
<?php
$attr_submit = array('class'=>'btn btn-primary');
if (isset($status) && mb_strtolower($status)=='concluído') {
    $input_attr['disabled'] = 'true';
    $attr_submit['disabled'] = 'true';
}
    
echo '<div class="form-group">';
echo form_label('Nome','nome');
echo form_input('nome', $values['nome'], $input_attr);
echo '</div>';

echo '<div class="form-group">';
echo form_label('Descrição','descricao');
echo form_textarea('descricao', $values['descricao'], $input_attr);
echo '</div>';

$date_attr = $input_attr;
$date_attr['class'] .= ' date';
echo '<div class="form-group">';
echo form_label('Data de início','data_inicio');
echo form_input('data_inicio', $values['data_inicio'], $date_attr);
echo '</div>';

echo '<div class="form-group">';
echo form_label('Data de fim','data_fim');
echo form_input('data_fim', $values['data_fim'], $date_attr);
echo '</div>';

$status_attr = $input_attr;
$status_attr['id'] = 'status';
echo '<div class="form-group">';
echo form_label('Status','status');
echo form_dropdown('status', $status_options,$values['status_id'], $status_attr);
echo '</div>';

echo '<div class="form-group">';
echo form_label('Situacao','situacao');
echo form_dropdown('situacao', $situacao_options,$values['situacao'], $input_attr);
echo '</div>';
?>

    <?php echo form_submit('', 'Enviar dados', $attr_submit);?>
<?php
echo form_close();