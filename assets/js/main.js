'use strict';

function searchFunction() {
    var input1, input2, filter1, filter2, table, tr, td1, td2, i;
    input1 = $("#searchInput-1");
    input2 = $("#searchInput-2");
    filter1 = input1.children(':selected').text().toUpperCase();
    filter2 = input2.children(':selected').text().toUpperCase();
    table = document.getElementById("atividades");
    tr = table.getElementsByTagName("tr");

    //Percorre todas as linhas e esconde as que não correspondem aos critérios de pesquisa combinados
    for (i = 1; i < tr.length; i++) {
        td1 = tr[i].getElementsByTagName("td")[4];
        td2 = tr[i].getElementsByTagName("td")[5];
        if (td1 && td2) {
            if ((filter1 != 'TODOS' && td1.innerHTML.toUpperCase() != filter1) || (filter2 != 'TODOS' && td2.innerHTML.toUpperCase() != filter2)) {
                tr[i].style.display = "none";
            } else {
                tr[i].style.display = "";
            }
        } 
    }
}

$(document).ready(function(){    
    $('.editar').on('click',function(){
        var id = $(this).data('id');
        location.href = './registro/'+id;
    });
    
    $('.novo').on('click',function(){
        location.href = './registro/';
    });
    
    $('#searchInput-1, #searchInput-2').on('change',searchFunction);
    
    $('#form_atividade').on('submit',function(e){
        
        if ($("#status").children(':selected').text() == "Concluído") {
            if ($('input[name="data_fim"]').val().length < 10) {
                e.preventDefault();
                $('#validation_errors').removeClass('hide_validation_errors');
                $('#validation_errors').addClass('show_validation_errors');
                $('#validation_errors').html('É necessário preencher uma Data de Fim válida ao tentar concluir uma atividade.');
                return false;
            } else {
                return true;
            }
            
        } else {
            return true;
        }
    });
});


